const cacheName = 'keep-secret-v1.1.3';
const cacheFiles = [
  '/',
  '/index.html',
  '/favicon.ico',
  '/assets/img/app-36x36.png',
  '/assets/img/app-48x48.png',
  '/assets/img/app-72x72.png',
  '/assets/img/app-96x96.png',
  '/assets/img/app-144x144.png',
  '/assets/img/app-168x168.png',
  '/assets/img/app-180x180.png',
  '/assets/img/app-192x192.png',
  '/assets/img/app-512x512.png'
];

self.addEventListener('install', installEvent => {
  installEvent.waitUntil(
    caches.open(cacheName).then(cache => {
      cache.addAll(cacheFiles);
    })
  );
});

self.addEventListener('message', function (event) {
  if (event.data.action === 'skipWaiting') {
    self.skipWaiting();
  }
});

self.addEventListener('fetch', fetchEvent => {
  fetchEvent.respondWith(
    caches.match(fetchEvent.request).then(res => {
      return res || fetch(fetchEvent.request);
    })
  );
});
