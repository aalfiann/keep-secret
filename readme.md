# KeepSecret
Simply encrypt your text.  
This tool will help you to keep your text always secret.  
https://keep-secret.netlify.app/

### Background
I've created this tool because I want to safely put my seeds phrase wallet to cloud service.  
I can't use online encrypt tool out there, because there is a risk if their website go shutdown I'll lose my seeds.  
So encrypt tool should be downloadable, installable, standalone, work offline and open source.

### How this work ?
This tool using AES-128/AES-192 (randomly based on length) but automatically use AES-256 (if use secret key) encryption + obfuscation with breaks the structure and include a random string to its content.  
So the encryption result will become a broken Base64.

### Features
- Built as PWA
- Worked Offline
- Clean (no ads, no tracker)
- Cross Platform (Windows, Linux, MacOS, Android, iOS)

### Disclaimer
Using this tool means you have already understand how this tool working and understand the risks.  
I believe that there is no 100% secure system. Please always keep treat your secret safely even it was already encrypted.

### Credits
- [CryptoJS v3.1.2](https://code.google.com/p/crypto-js)
- [Bulma v0.9.4](https://bulma.io)
- [FontAwesome v6](https://fontawesome.io)

### Support Me
I've accept cryptocurrency $XEC #eCash  
![](https://i.imgur.com/QCGuEYj.png)  
ecash:qqcn5j6ej57xngsscn7ugm80un4jujk72qypcm8gyl

### MIT License
Copyright 2023 M ABD AZIZ ALFIAN

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.